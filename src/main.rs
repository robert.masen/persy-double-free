use std::time::SystemTime;

use structsy_derive::{Persistent, PersistentEmbedded};
use structsy::{Structsy, StructsyTx};
use rand::prelude::*;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    insert_random_things()?;
    let db = Structsy::open("file.db")?;
    db.define::<Thing>()?;
    for (_id, item) in db.query::<Thing>() {
        println!("{:?}", item)
    }
    Ok(())
}

pub fn insert_random_things() -> Result<(), Box<dyn std::error::Error>> {
    let db = Structsy::open("file.db")?;
    db.define::<Thing>()?;
    let timestamp = SystemTime::now().duration_since(std::time::UNIX_EPOCH)?;
    let mut rng = thread_rng();
    for _ in 0..10 {
        let id = uuid::Uuid::new_v4().to_string();
        let sub = if rng.gen() {
            Sub::One(rng.gen())
        } else {
            Sub::Two(rng.gen())
        };
        let thing = Thing {
            timestamp: timestamp.as_secs(),
            id,
            sub,
        };
        let mut tx = db.begin()?;
        tx.insert(&thing)?;
        tx.commit()?;
    }
    Ok(())
}

#[derive(Debug, Persistent)]
pub struct Thing {
    #[index(mode="cluster")]
    pub timestamp: u64,
    #[index(mode="exclusive")]
    id: String,
    sub: Sub,
}

#[derive(Debug, PersistentEmbedded)]
pub enum Sub {
    One(u64),
    Two(f64)
}
